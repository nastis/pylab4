from functools import reduce

__author__ = 'mitroa'

def opn(line, i):
    it = len(line)
    if i == "+":
        res = line[it-2] + line[it-1]
        return line[:it-2] + (res,)
    elif i == "-":
        res = line[it-2] - line[it-1]
        return line[:it-2] + (res,)
    elif i == "*":
        res = line[it-2] * line[it-1]
        return line[:it-2] + (res,)
    elif i == "/":
        if line[it-1] != 0:
            res = line[it-2] / line[it-1]
            return line[:it-2] + (res,)
        else:
            raise ZeroDivisionError("Do not divide by zero")
    elif i == "^":
        return line[:it-1] + (-line[it-1],)
    elif i.isdigit():
        return line + (float(i),)
    else:
        raise ValueError("Wrong symbol")



# ln = ("10", "5", "5", "-", "/")
# ln = ("10", "a", "^", "+")
ln = ("10", "5", "^", "+")
try:
    print(reduce(opn, ln, ())[0])
except ZeroDivisionError:
    print("You tried to divide by zero")
except ValueError:
    print("Wrong symbols detected")